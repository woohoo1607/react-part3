const chat = require('../data.json');
const uuid = require('uuid');
const fs = require('fs');
const path = require('path');
const chatFile = path.join(__dirname, '../data.json')

class chatController {
  static async index (req, res, next) {
    res.status(200).json(chat)
  }
  static async write (req, res, next) {
    let newMessage = req.body;
    newMessage.id = uuid();
    let rawdata = fs.readFileSync(chatFile);
    let chatData = JSON.parse(rawdata)
    chatData.push(newMessage);
    fs.writeFileSync(chatFile, JSON.stringify(chatData));
    res.status(200).json(chatData)
  }
  static async update (req, res, next) {
    let reqMessage = req.body;
    let rawdata = fs.readFileSync(chatFile);
    let chatData = JSON.parse(rawdata)
    let messageIndex = chatData.findIndex(msg => msg.id===reqMessage.id);
    chatData[messageIndex].text = reqMessage.text;
    chatData[messageIndex].editedAt = reqMessage.editedAt;
    fs.writeFileSync(chatFile, JSON.stringify(chatData));
    res.status(200).json(chatData)
  }
  static async delete (req, res, next) {
    let rawdata = fs.readFileSync(chatFile);
    let chatData = JSON.parse(rawdata)
    let messageIndex = chatData.findIndex(msg => msg.id===req.params.id);
    chatData.splice(messageIndex,1);
    fs.writeFileSync(chatFile, JSON.stringify(chatData));
    res.status(200).json(chatData)
  }
}
module.exports = chatController;
