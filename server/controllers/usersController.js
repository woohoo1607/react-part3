const users = require('../users.json');
const uuid = require('uuid');
const fs = require('fs');
const path = require('path');
const usersFile = path.join(__dirname, '../users.json')

class usersController {
  static async index (req, res, next) {
    res.status(200).json(users)
  }
  static async read (req, res, next) {
    let rawdata = fs.readFileSync(usersFile);
    let user = JSON.parse(rawdata).filter(user => user.userId ===req.params.id);
    res.status(200).json(user)
  }
  static async write (req, res, next) {
    let newUser = req.body;
    newUser.userId = uuid();
    let rawdata = fs.readFileSync(usersFile);
    let usersData = JSON.parse(rawdata)
    usersData.push(newUser);
    fs.writeFileSync(usersFile, JSON.stringify(usersData));
    res.status(200).json(usersData)
  }
  static async update (req, res, next) {
    let reqUser = req.body;
    let rawdata = fs.readFileSync(usersFile);
    let usersData = JSON.parse(rawdata)
    let userIndex = usersData.findIndex(user => user.userId===reqUser.userId);
    usersData[userIndex] = reqUser;
    fs.writeFileSync(usersFile, JSON.stringify(usersData));
    res.status(200).json(usersData)
  }
  static async delete (req, res, next) {
    let rawdata = fs.readFileSync(usersFile);
    let usersData = JSON.parse(rawdata)
    let userIndex = usersData.findIndex(user => user.userId===req.params.userId);
    usersData.splice(userIndex,1);
    fs.writeFileSync(usersFile, JSON.stringify(usersData));
    res.status(200).json(usersData)
  }
}
module.exports = usersController;
