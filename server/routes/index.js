const users = require("./users");
const auth = require("./auth");
const chat = require("./chat");

module.exports = (app) => {
  app.use(users);
  app.use(auth);
  app.use(chat);
};
