const express = require('express');
const router = express.Router();

const chatController = require("../controllers/chatController");

router.get('/chat', chatController.index);
router.post('/chat', chatController.write);
router.put('/chat', chatController.update);
router.delete('/chat/:id', chatController.delete);


module.exports = router;
