export const requiredField = value => value ? undefined : "Это поле обязательное для заполнения";

const minLength = max => value => value && value.length < max ? `Минимальное число символов = ${max}` : undefined;

export const minLength8 = minLength(8);
