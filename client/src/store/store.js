import {applyMiddleware, combineReducers, createStore, compose} from "redux";
import createSagaMiddleware from "redux-saga";
import thunkMiddleware from "redux-thunk";
import {reducer as formReducer} from "redux-form";

import userReducer from "../reducers/userReducer";
import messengerReducer from "../reducers/messengerReducer";
import rootSaga from "../sagas";
import popupReducer from "../reducers/popupReducer";

const sagaMiddleware = createSagaMiddleware()
let reducers = combineReducers({
  user: userReducer,
  messenger: messengerReducer,
  popup: popupReducer,
  form: formReducer,
});
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducers, composeEnhancers(applyMiddleware(thunkMiddleware, sagaMiddleware)));
sagaMiddleware.run(rootSaga);

export default store;
