import React from 'react';
import { TextField } from '@material-ui/core';

export const renderTextField = ({label, input, meta: {touched, invalid, error}, ...props}) => {
  return <TextField label={label}
  placeholder={label}
  error={touched && invalid}
  helperText={touched && error}
  fullWidth={true}
  {...input}
  {...props}
  />
};
