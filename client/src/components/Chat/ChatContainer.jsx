import React, {useEffect} from "react";
import {connect} from "react-redux";
import Chat from "./Chat";
import {
  deleteMessage,
  fetchMessages,
  sendMessage,
  setIsShowModal,
  setLikes,
  setMessages
} from "../../reducers/messengerReducer";
import {getDate} from "../../helpers/helpers";
import {withRouter} from "react-router-dom";

const ChatContainer = (props) => {
  useEffect(() => {
    props.fetchMessages()
  },[])

  const deleteMsg = (msgId) => {
    props.deleteMessage(msgId);
  }

  const sendMsg = (msg) => {
    if (!msg.length) {
      return
    }
    const newMessage = {
      text: msg,
      avatar: props.user.avatar,
      user: props.user.user,
      userId: props.user.userId,
      editedAt: "",
      createdAt: getDate()
    }
    props.sendMessage(newMessage);
  }

  const liked = (msgId, userId) => {
    let copyLikes = [...props.likes];
    const likeIndex = copyLikes.findIndex(like=> like.msgId===msgId && like.userId===userId);
    if (likeIndex===-1) {
      const newLike = {
        msgId,
        userId,
        isLike: true,
      };
      copyLikes.push(newLike);
      props.setLikes(copyLikes);
    } else {
      copyLikes.splice(likeIndex, 1);
      props.setLikes(copyLikes);
    }
  }
  if (!props.isAuth) {
    props.history.push('/login')
  }

  return (
      <Chat myId={props.user.userId}
            messages={props.messages}
            likes={props.likes}
            isFetching={props.isFetching}
            deleteMsg={deleteMsg}
            sendMsg={sendMsg}
            liked={liked}
            isShowModal={props.isShowModal}
            historyPush={props.history.push}
      />
  )
};

let mapStateToProps = (state) => {
  return {
    user: state.user.user,
    messages: state.messenger.messages,
    likes: state.messenger.likes,
    isFetching: state.messenger.isFetching,
    isAuth: state.user.isAuth,
  }
};

export default connect(mapStateToProps, {setMessages, setLikes, setIsShowModal, fetchMessages, sendMessage, deleteMessage})(withRouter(ChatContainer));
