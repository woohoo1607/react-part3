import React, {useState} from 'react';

const ChatFooter = ({sendMsg}) => {
  let [msg, setMsg] = useState('');
  const send = () => {
    if (msg.length===0) {
      return false
    }
    sendMsg(msg);
    setMsg('');
  };
  const changeInput = (e)  => {
    setMsg(e.target.value);
  }
  return (
      <div className='chatFooter'>
        <textarea onChange={changeInput} value={msg} placeholder='Write a message...'/>
        <button onClick={send}>Send</button>
      </div>
  )
};

export default ChatFooter;
