import React from "react";
import {connect} from "react-redux";

import "./styles.css";

const PopUp = (props) => {
  return (
      <>
        {props.isOpenPopup && <div className="popup">
          <p>{props.popupMsg}</p>
        </div>}
      </>
  )
};

let mapStateToProps = (state) => {
  return {
    popupMsg: state.popup.popupMsg,
    isOpenPopup: state.popup.isOpenPopup,
  }
};

export default connect(mapStateToProps, {})(PopUp);
