import React, {useState} from "react";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import EditMessage from "./EditMessage";
import {updateMessage} from "../../reducers/messengerReducer";
import {getDate} from "../../helpers/helpers";

const EditMessageContainer = (props) => {

  let [msg, setMsg] = useState(props.match.params.message);

  const changeInput = (e)  => {
    setMsg(e.target.value);
  }

  const cancelEdit = () => {
    props.history.goBack()
  }

  const meta = {
    redirect: props.history.push,
    path: '/'
  }

  const saveMsg = () => {
    if (msg.length===0) {
      props.history.goBack()
    } else {
      const message = {
        id: props.match.params.id,
        text: msg,
        editedAt: getDate()
      }
      props.updateMessage(message, meta)
    }
  }

  return (
      <EditMessage msg={msg}
                   changeInput={changeInput}
                   cancelEdit={cancelEdit}
                   saveMsg={saveMsg}
      />
  )
};

let mapStateToProps = (state) => {
  return {

  }
};

export default connect(mapStateToProps, {updateMessage})(withRouter(EditMessageContainer))
