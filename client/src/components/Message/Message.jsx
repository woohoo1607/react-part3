import React, {useState, useEffect} from 'react';
import Moment from 'react-moment';
import 'moment-timezone';

import Like from '../../image/like.png';
import './styles.css';
import Avatar from './no-avatar.png';

const Message = ({message, myId, deleteMsg, liked, messageLikes, isSuccessEdit, historyPush, ...props}) => {

  const upKeyPress = (e) => {
    if(e.key==='ArrowUp') {
      editMsg()
    }
  }

  useEffect(() => {
    if (isSuccessEdit) {
      document.addEventListener("keydown", upKeyPress);
    }
    return () => {
      document.removeEventListener("keydown", upKeyPress);
    }
  },[isSuccessEdit]);

  let [isMouseEnter, setIsMouseEnter] = useState(false);
  let isLiked = false;
  if (messageLikes.find(like=> like.userId===myId)) {
    isLiked=true
  }

  const mouseEnter = () => {
    setIsMouseEnter(true);
  }

  const mouseLeave = () => {
    setIsMouseEnter(false);
  }

  const editMsg = () => {
    historyPush(`/edit/${message.id}/${message.text}`)
  }

  const deleteMessage = () => {
    deleteMsg(message.id);
  }

  const likeMsg = () => {
    liked(message.id, myId)
  }

  let isMyMsg = message.userId===myId;
  return (
      <>
      <div className={isMyMsg ? 'message right' : 'message'} onMouseEnter={mouseEnter} onMouseLeave={mouseLeave}>
        {!isMyMsg && <>
          {message.avatar.length>0 && <img src={message.avatar} alt='avatar'/>}
          {message.avatar.length===0 && <img src={Avatar} alt='no-avatar'/>}
          </>
        }
        <div className='bodyMessage'>
          <div className='bodyMessageTitle'>
            <h3>{message.user}</h3>
            <p><Moment format='hh:mm:ss' date={message.createdAt}/></p>
            {isMyMsg && <>
              {isSuccessEdit && isMouseEnter && <button onClick={editMsg}>edit</button>}
                <button onClick={deleteMessage}>delete</button>
              </>
            }
          </div>
          <p>{message.text}</p>
          <div className='messageInfo'>
            {isLiked && <span>you like</span>}
            {!isMyMsg && <img onClick={likeMsg} src={Like} height='10px' alt='like'/>}
          </div>
        </div>
      </div>
        </>
  )
};
export default Message;
