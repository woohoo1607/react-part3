import React from "react";

import './styles.css';
import Preloader from "../Preloader/Preloader";
import {Field, reduxForm} from "redux-form";
import {renderTextField} from "../FormsControls/FormsControls";
import {requiredField, minLength8} from "../../validators/validators";

const EditUserForm = (props) => {
  return (
      <form onSubmit={props.handleSubmit}>
        <div className="inputContainer">
          <Field name="user" component={renderTextField} label="username" type="text" validate={[requiredField]}/>
        </div>
        <div className="inputContainer">
          <Field name="password" component={renderTextField} label="password" type="password" validate={[requiredField, minLength8]}/>
        </div>
        <button type="submit" className="login-btn" disabled={props.isFetching}>
          {props.isNewUser ? 'Create' : 'Edit'}
        </button>
      </form>
  )
};

const EditUserReduxForm = reduxForm({form:'editUserForm'})(EditUserForm);

const EditUser = ({isNewUser, isFetching, currentUser, isUserError, msgUserError, createUser, editUser, historyPush}) => {
  const onSubmit = (formData) => {
    if (isNewUser) {
      formData.avatar = '';
      formData.isAdmin = false;
      createUser(formData)
    } else {
      editUser(formData)
    }
  };
  return (
      <div className='center'>
        <div className='editUser'>
          <h2>{isNewUser ? 'Create' : 'Edit'} user</h2>
          <p className="login-error">{msgUserError}</p>
          {!isFetching && <EditUserReduxForm onSubmit={onSubmit} isFetching={isFetching} isNewUser={isNewUser} initialValues={currentUser}/>}
          {isFetching && <Preloader />}
        </div>
      </div>
  )
};
export default EditUser;
