import React, {useEffect} from "react";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import EditUser from "./EditUser";
import {
  requestCreateUser,
  requestCurrentUser,
  requestDeleteUser,
  requestEditUser,
  setCurrentUser
} from "../../reducers/userReducer";

const EditUserContainer = (props) => {

  useEffect(() => {
    if (props.match.params.id!=='new') {
      props.requestCurrentUser(props.match.params.id)
    }
    return (()=> {
      props.setCurrentUser({})
    })
  },[])

  const meta = {
    redirect: props.history.push,
    path: '/users'
  }

  const editUser = (user) => {
    props.requestEditUser(user, meta);
  }

  const createUser = (user) => {
    props.requestCreateUser(user, meta);
  }

  if (!props.isAuth) {
    props.history.push('/login')
  }

  if (props.isAuth && !props.user.isAdmin) {
    props.history.push('/')
  }

  return (
      <EditUser isFetching={props.isFetching}
                currentUser={props.currentUser}
                isUserError={props.isUserError}
                msgUserError={props.msgUserError}
                isNewUser={!props.currentUser.hasOwnProperty('userId')}
                editUser={editUser}
                createUser={createUser}
                historyPush={props.history.push}
      />
  )
};

let mapStateToProps = (state) => {
  return {
    user: state.user.user,
    isFetching: state.user.isFetching,
    isAuth: state.user.isAuth,
    currentUser: state.user.currentUser,
    isUserError: state.user.isUserError,
    msgUserError: state.user.msgUserError,
  }
};
export default connect(mapStateToProps, {requestCurrentUser, setCurrentUser, requestEditUser, requestCreateUser, requestDeleteUser})(withRouter(EditUserContainer))
