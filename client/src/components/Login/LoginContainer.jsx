import React, {useEffect} from "react";
import {connect} from "react-redux";
import {Redirect, withRouter} from "react-router-dom";

import Login from "./Login";
import {loginUser} from "../../reducers/userReducer";

const LoginContainer = (props) => {

  useEffect(()=> {

  }, []);

  const logIn = (user, password) => {
    props.loginUser({user, password});
  };
  return (
      <>
        {props.isAuth && !props.user.isAdmin && <Redirect to='/' />}
        {props.isAuth && props.user.isAdmin && <Redirect to='/users' />}
        <Login {...props}
               logIn={logIn}

        />
      </>
  )
};


let mapStateToProps = (state) => {
  return {
    user: state.user.user,
    isAuth: state.user.isAuth,
    isFetching: state.user.isFetching,
    isUserError: state.user.isUserError,
    msgUserError: state.user.msgUserError
  }
};

export default connect(mapStateToProps, {loginUser})(withRouter(LoginContainer));
