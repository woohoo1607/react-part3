import React from 'react';
import {NavLink} from "react-router-dom";

import Logo from '../../image/messengerLogo.png';
import './styles.css';

const Header = () => {
  return (
      <header>
        <div className="center">
          <img src={Logo} height="70px" alt="logo" />
          <h2>Messenger</h2>
        </div>
        <nav>
          <ul>
            <li><NavLink to="/">Chat</NavLink></li>
            <li><NavLink to="/users">Users</NavLink></li>
          </ul>
        </nav>
        <div className='clr'></div>
      </header>
  )
};
export default Header;
