export const SET_IS_FETCHING = 'SET_IS_FETCHING';
export const SET_MESSAGES = 'SET_MESSAGES';
export const SET_LIKES = 'SET_LIKES';
export const SET_IS_SHOW_MODAL = 'SET_IS_SHOW_MODAL';
export const FETCH_MESSAGES = 'FETCH_MESSAGES';
export const SEND_MESSAGE = 'SEND_MESSAGE';
export const UPDATE_MESSAGE = 'UPDATE_MESSAGE';
export const DELETE_MESSAGE = 'DELETE_MESSAGE';
const IS_CHAT_ERROR = 'IS_CHAT_ERROR';
const MSG_CHAT_ERROR = 'MSG_CHAT_ERROR';

let initialState = {
  messages: [],
  likes: [],
  isFetching: false,
  isShowModal: false,
  isChatError: false,
  msgChatError: '',
};

const messengerReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_MESSAGES:
    {
      return {
        ...state,
        messages: [...action.messages]
      };
    }
    case SET_LIKES:
    {
      return {
        ...state,
        likes: [...action.likes]
      };
    }
    case SET_IS_FETCHING:
    {
      return {
        ...state,
        isFetching: action.isFetching
      };
    }
    case SET_IS_SHOW_MODAL: {
      return {
        ...state,
        isShowModal: action.isShowModal
      };
    }
    case IS_CHAT_ERROR:
    {
      return {
        ...state,
        isChatError: action.isChatError
      };
    }
    case MSG_CHAT_ERROR:
    {
      return {
        ...state,
        msgChatError: action.msgChatError
      };
    }
    default:
      return state;
  }
};

export const setMessages = (messages) =>
    ({type: SET_MESSAGES, messages: messages});

export const setLikes = (likes) =>
    ({type: SET_LIKES, likes: likes});

export const setIsFetching = (isFetching) =>
    ({type: SET_IS_FETCHING, isFetching: isFetching});

export const setIsShowModal = (isShowModal) =>
    ({type: SET_IS_SHOW_MODAL, isShowModal: isShowModal});

export const setIsChatError = (isChatError) =>
    ({type: IS_CHAT_ERROR, isChatError: isChatError});

export const setMsgChatError = (msgChatError) =>
    ({type: MSG_CHAT_ERROR, msgChatError: msgChatError});

export const fetchMessages = () =>
    ({type: FETCH_MESSAGES})

export const sendMessage = (msg) =>
    ({type: SEND_MESSAGE, msg})

export const updateMessage = (msg, meta) =>
    ({type: UPDATE_MESSAGE, msg, meta})

export const deleteMessage = (id) =>
    ({type: DELETE_MESSAGE, id})

export default messengerReducer;
