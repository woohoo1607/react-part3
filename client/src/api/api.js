import axios from 'axios';
import qs from 'qs';

const instance = axios.create({
  baseURL: 'http://localhost:5000/',
});

export const chatAPI = {
  getChatData () {
    return instance.get('chat').then(res => {
      return res.data
    }).catch(err=> JSON.parse(err.request.responseText));
  },
  postMsg (msg) {
    const data = qs.stringify(msg);
    return instance.post(`chat`, data).then(response => {
      return response.data
    }).catch(err=> JSON.parse(err.request.responseText));
  },
  updateMsg(msg) {
    const data = qs.stringify(msg);
    return instance.put(`chat`, data).then(response => {
      return response.data
    }).catch(err=> JSON.parse(err.request.responseText));
  },
  deleteMsg(id) {
    return instance.delete(`chat/${id}`).then(response => {
      return response.data
    }).catch(err=> JSON.parse(err.request.responseText));
  },
};

export const userAPI = {
  getUsers() {
    return instance.get('users').then(res => {
      return res.data
    }).catch(err=> JSON.parse(err.request.responseText));
  },
  getUser(id) {
    return instance.get(`users/${id}`).then(res => {
      return res.data
    }).catch(err=> JSON.parse(err.request.responseText));
  },
  postUser(user) {
    const data = qs.stringify(user);
    return instance.post(`users`, data).then(response => {
      return response.data
    }).catch(err=> JSON.parse(err.request.responseText));
  },
  updateUser(user) {
    const data = qs.stringify(user);
    return instance.put(`users`, data).then(response => {
      return response.data
    }).catch(err=> JSON.parse(err.request.responseText));
  },
  deleteUser(id) {
    return instance.delete(`users/userId=${id}`).then(response => {
      return response.data
    }).catch(err=> JSON.parse(err.request.responseText));
  },
};

export const authAPI = {
  login (user, password) {
    const data = qs.stringify({user, password});
    return instance.post(`login`, data).then(response => {
      return response.data
    }).catch(err=> JSON.parse(err.request.responseText));
  },
  me (userId) {
    return instance.get(`me?userId=${userId}`).then(res => {
      return res.data
    }).catch(err=> JSON.parse(err.request.responseText));
  }
}
