import {call, put, takeEvery, all, delay} from "redux-saga/effects";
import {authAPI, userAPI} from "../api/api";
import {
  CHANGE_USER_ERROR,
  LOGIN_USER, REQUEST_CREATE_USER,
  REQUEST_CURRENT_USER,
  REQUEST_DELETE_USER,
  REQUEST_EDIT_USER,
  REQUEST_USERS_LIST,
  setCurrentUser,
  setIsAuth,
  setIsFetchingUser,
  setIsUserError,
  setMsgUserError,
  setUser,
  setUsersLists
} from "../reducers/userReducer";
import {openPopUp} from "../reducers/popupReducer";

export function* loginUser({data}) {
  try {
    yield put(setIsFetchingUser(true))
    yield put({type: CHANGE_USER_ERROR, msg: ''})
    const user = yield call(authAPI.login, data.user, data.password)
    if (user.length===1) {
      yield put(setUser(user[0]))
      yield put(setIsAuth(true))
    } else {
      yield put({type: CHANGE_USER_ERROR, msg: 'Неверный логин или пароль'})
      yield put(setIsAuth(false))
    }
    yield put(setIsFetchingUser(false))
  } catch (error) {
    console.log(error.message)
  }
}

function* watchLoginUser() {
  yield takeEvery(LOGIN_USER, loginUser)
}

export function* changeUserError({msg}) {
  if (msg.length===0) {
    yield put(setIsUserError(false))
  } else {
    yield put(setIsUserError(true))
    yield put(openPopUp(msg))
  }
  yield put(setMsgUserError(msg))
}

function* watchErrorUser() {
  yield takeEvery(CHANGE_USER_ERROR, changeUserError)
}

export function* getUsersList() {
  try {
    yield put(setIsFetchingUser(true))
    yield put({type: CHANGE_USER_ERROR, msg: ''})
    const users = yield call(userAPI.getUsers)
    yield put(setUsersLists(users))
    yield put(setIsFetchingUser(false))
  } catch (error) {
    console.log(error.message)
  }
}

function* watchGetUsersList() {
  yield takeEvery(REQUEST_USERS_LIST, getUsersList)
}

export function* getUser({id}) {
  try {
    yield put(setIsFetchingUser(true))
    yield put({type: CHANGE_USER_ERROR, msg: ''})
    const user = yield call(userAPI.getUser, id)
    if (user.length===1) {
      yield put(setCurrentUser(user[0]))
    } else {
      yield put({type: CHANGE_USER_ERROR, msg: 'Пользователь не найден'})
    }
    yield put(setIsFetchingUser(false))
  } catch (error) {
    console.log(error.message)
  }
}

function* watchGetUser() {
  yield takeEvery(REQUEST_CURRENT_USER, getUser)
}

export function* updateUser({user, meta}) {
  try {
    yield put(setIsFetchingUser(true))
    yield put({type: CHANGE_USER_ERROR, msg: ''})
    const users = yield call(userAPI.updateUser, user)
    yield put(setUsersLists(users))
    yield delay(300);
    yield call(meta.redirect, meta.path)
    yield put(setIsFetchingUser(false))
  } catch (error) {
    console.log(error.message)
  }
}

function* watchUpdateUser() {
  yield takeEvery(REQUEST_EDIT_USER, updateUser)
}

export function* createUser({user, meta}) {
  try {
    yield put(setIsFetchingUser(true))
    yield put({type: CHANGE_USER_ERROR, msg: ''})
    const users = yield call(userAPI.postUser, user)
    yield put(setUsersLists(users))
    yield delay(300);
    yield call(meta.redirect, meta.path)
    yield put(setIsFetchingUser(false))
  } catch (error) {
    console.log(error.message)
    yield put(openPopUp(error.message))
  }
}

function* watchCreateUser() {
  yield takeEvery(REQUEST_CREATE_USER, createUser)
}

export function* deleteUser({userId}) {
  try {
    yield put(setIsFetchingUser(true))
    yield put({type: CHANGE_USER_ERROR, msg: ''})
    const users = yield call(userAPI.deleteUser, userId)
    yield put(setUsersLists(users))
    yield put(setIsFetchingUser(false))
  } catch (error) {
    console.log(error.message)
    yield put(openPopUp(error.message))
  }
}

function* watchDeleteUser() {
  yield takeEvery(REQUEST_DELETE_USER, deleteUser)
}

export default function* userSagas() {
  yield all([
    watchLoginUser(),
    watchErrorUser(),
    watchGetUsersList(),
    watchDeleteUser(),
    watchGetUser(),
    watchUpdateUser(),
    watchCreateUser(),
  ])
}
