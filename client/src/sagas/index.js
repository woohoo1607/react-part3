import {all} from "redux-saga/effects";
import chatSagas from "./chatSagas";
import userSagas from "./userSagas";
import popUpSagas from "./popupSagas";

export default function* rootSaga() {
  yield all ([
    chatSagas(),
    userSagas(),
    popUpSagas(),
  ])
};
