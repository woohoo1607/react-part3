import {call, put, takeEvery, all, delay} from "redux-saga/effects";
import {chatAPI} from "../api/api";
import {
  DELETE_MESSAGE,
  FETCH_MESSAGES,
  SEND_MESSAGE,
  setIsFetching,
  setMessages,
  UPDATE_MESSAGE
} from "../reducers/messengerReducer";
import {sortByTime} from "../helpers/helpers";
import {openPopUp} from "../reducers/popupReducer";

export function* fetchMessages() {
  try {
    yield put(setIsFetching(true))
    const messages = yield call(chatAPI.getChatData)
    yield updateMessageState(messages)
    yield put(setIsFetching(false))
  } catch (error) {
    console.log(error.message)
    yield put(openPopUp(error.message))
  }
}

function* watchFetchMessages() {
  yield takeEvery(FETCH_MESSAGES, fetchMessages)
}

export function* sendMessage({msg}) {
  try {
    yield delay(100);
    const messages = yield call(chatAPI.postMsg, msg)
    yield updateMessageState(messages)
  } catch (error) {
    console.log(error.message)
    yield put(openPopUp(error.message))
  }
}

function* watchSendMessage() {
  yield takeEvery(SEND_MESSAGE, sendMessage)
}

export function* updateMessage({msg, meta}) {
  try {
    const messages = yield call(chatAPI.updateMsg, msg)
    yield updateMessageState(messages)
    yield delay(300);
    yield call(meta.redirect, meta.path)
  } catch (error) {
    console.log(error.message)
    yield put(openPopUp(error.message))
  }
}

function* watchUpdateMessage() {
  yield takeEvery(UPDATE_MESSAGE, updateMessage)
}

export function* deleteMessage({id}) {
  try {
    const messages = yield call(chatAPI.deleteMsg, id)
    yield updateMessageState(messages)
  } catch (error) {
    console.log(error.message)
    yield put(openPopUp(error.message))
  }
}

function* watchDeleteMessage() {
  yield takeEvery(DELETE_MESSAGE, deleteMessage)
}

export default function* chatSagas() {
  yield all([
    watchFetchMessages(),
    watchSendMessage(),
    watchUpdateMessage(),
    watchDeleteMessage(),
  ])
}

const updateMessageState = (messages) => {
  sortByTime(messages)
  return put(setMessages(messages))
}
