import {put, takeEvery, all, delay} from "redux-saga/effects";
import {changePopupStatus, CLOSE_POPUP, OPEN_POPUP, openPopUp, setPopupMsg} from "../reducers/popupReducer";

export function* viewPopUp({popupMsg}) {
  try {
    yield put(changePopupStatus(true))
    yield put(setPopupMsg(popupMsg))
    yield delay(6000)
    yield put({type: CLOSE_POPUP})
  } catch (error) {
    console.log(error.message)
    yield put(openPopUp(error.message))
  }
}

function* watchOpenPopUp() {
  yield takeEvery(OPEN_POPUP, viewPopUp)
}

export function* closePopUp() {
  try {
    yield put(changePopupStatus(false))
    yield put(setPopupMsg(''))
    yield delay(6000)
  } catch (error) {
    console.log(error.message)
    yield put(openPopUp(error.message))
  }
}

function* watchClosePopUp() {
  yield takeEvery(CLOSE_POPUP, closePopUp)
}

export default function* popUpSagas() {
  yield all([
    watchOpenPopUp(),
    watchClosePopUp()
  ])
};
